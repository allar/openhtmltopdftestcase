# OPEN HTML TO PDF

Describing an issue with https://github.com/danfickle/openhtmltopdf version 0.0.1-RC16

When text is justified and background color is set, the background is rendered incorrectly.
See sample output file for result [sample/out.pdf](https://bitbucket.org/allar/openhtmltopdftestcase/src/master/sample/out.pdf)