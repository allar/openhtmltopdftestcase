package com.openhtmltopdf.testcase;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import java.io.*;

public class PdfGenerator {


    public static void main(String[] args) throws Exception {
        String html = "<html>\n" +
                "<head>\n" +
                "    <style>\n" +
                "        .content {\n" +
                "            text-align: justify;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div class=\"content\">\n" +
                "<p><span style=\"background-color:#1abc9c\">\n" +
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam faucibus neque et arcu finibus consectetur. Vivamus tempor erat non enim malesuada vulputate. Vestibulum ut felis at velit convallis vehicula non sed libero. Duis euismod iaculis lacus at congue. Cras interdum odio ac tellus vehicula consectetur. Suspendisse quis nisi leo. Ut lobortis malesuada nunc, eget condimentum libero ornare nec. Cras pulvinar eros in ligula tincidunt blandit. Donec sed nisi lectus. Duis ligula elit, dignissim quis ullamcorper id, pellentesque ut nulla. Suspendisse dictum eu turpis id ullamcorper. Sed commodo massa sed sollicitudin vehicula. Quisque eleifend varius dolor, vel commodo mi porttitor ac. Aenean id nisl elit. Nulla consectetur in urna non molestie.</span></p>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        try (OutputStream os = new FileOutputStream("out.pdf")) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(html, null);
            builder.toStream(os);
            builder.run();
        }
    }
}
